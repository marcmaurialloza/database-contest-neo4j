import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.AutoIndexer;
import org.neo4j.graphdb.index.ReadableIndex;


public class Main {
	
	private static enum RelTypes implements RelationshipType
	{
	    HAS_NATION,
	    HAS_REGION,
	    HAS_CUSTOMER,
	    HAS_PART,
	    HAS_SUPPLIER,
	    HAS_ORDER,
	    HAS_PART_SUP
	    
	};
	
	private static String randomString(int length, Random random) {
        String c = new String();
        String caracteres = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < length; ++i) {
            c += caracteres.charAt(random.nextInt(caracteres.length()));
        }
        return c;
    }
    
    private static long randomDate(Random random) throws ParseException {
        int anyo, mes, dia;
        anyo = random.nextInt(14) + 2000;
        mes = random.nextInt(12) + 1;
        dia = random.nextInt(28) + 1;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(String.valueOf(anyo)+"-"+String.valueOf(mes)+"-"+String.valueOf(dia) ).getTime();
    }
	
	private static void insertRegion(GraphDatabaseService gds, String regionKey, String name, String comment) {
		Node region = gds.createNode();
		
		region.setProperty("rRegionKey", regionKey);
		region.setProperty("rName", name);
		region.setProperty("rComment", comment);
		
	}
	
	private static void insertPart(GraphDatabaseService gds, String partKey,
			String name, String mfgr, String brand, String type,
			String size, String container, String retailPrice, String comment) {
		Node part = gds.createNode();
		
		part.setProperty("pPartKey", partKey);
		part.setProperty("pName", name);		
		part.setProperty("pMfgr", mfgr);		
		part.setProperty("pBrand", brand);		
		part.setProperty("pType", type);		
		part.setProperty("pSize", size);		
		part.setProperty("pContainer", container);		
		part.setProperty("pRetailPrice", retailPrice);		
		part.setProperty("pComment", comment);		
		
	}
	
	private static void insertNation(GraphDatabaseService gds, String nationKey, String name, String regionKey, String comment) {		
		
		Node nation= gds.createNode();
		
		nation.setProperty("nNationKey", nationKey);
		nation.setProperty("nName", name);
		nation.setProperty("nComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node region  = autoNodeIndex.get("rRegionKey", regionKey).getSingle();
		nation.createRelationshipTo(region, RelTypes.HAS_REGION);
		
	}
	
	private static void insertSupplier(GraphDatabaseService gds, String supKey,
			String name, String address, String nationKey, String phone,
			String acctBal, String comment) {
		Node supplier = gds.createNode();
		supplier.setProperty("sSupKey", supKey);
		supplier.setProperty("sName", name);
		supplier.setProperty("sAddress", address);
		supplier.setProperty("sPhone", phone);
		supplier.setProperty("sAcctBal", acctBal);
		supplier.setProperty("sComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node nation = autoNodeIndex.get("nNationKey", nationKey).getSingle();
		supplier.createRelationshipTo(nation, RelTypes.HAS_NATION);
		
	}
	
	private static void insertCustomer(GraphDatabaseService gds, String custKey,
			String name, String address, String nationKey, String phone,
			String acctBal, String mktSegment, String comment) {
		
		Node customer = gds.createNode();
		customer.setProperty("cCustKey", custKey);
		customer.setProperty("cName", name);
		customer.setProperty("cAddress", address);
		customer.setProperty("cPhone", phone);
		customer.setProperty("cAcctBal", acctBal);
		customer.setProperty("cMktSegment", mktSegment);
		customer.setProperty("cComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node nation  = autoNodeIndex.get("nNationKey", nationKey).getSingle();
		customer.createRelationshipTo(nation, RelTypes.HAS_NATION);
	}
	
	private static void insertOrder(GraphDatabaseService gds, String orderKey,
			String custKey, String orderStatus, String totalPrice, long orderDate,
			String orderPriority, String clerk, String shipPriority, String comment) {
		
		Node order = gds.createNode();
		
		order.setProperty("oOrderKey", orderKey);
		order.setProperty("oCustKey", custKey);
		order.setProperty("oOrderStatus", orderStatus);
		order.setProperty("oTotalPrice", totalPrice);
		order.setProperty("oOrderDate", orderDate);
		order.setProperty("oOrderPriority", orderPriority);
		order.setProperty("oClerk", clerk);
		order.setProperty("oShipPriority", shipPriority);
		order.setProperty("oComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node customer  = autoNodeIndex.get("cCustKey", custKey).getSingle();
		order.createRelationshipTo(customer, RelTypes.HAS_CUSTOMER);
		
	}
	
	private static void insertPartSup(GraphDatabaseService gds, String partKey,
			String supKey, String availQty, String supplyCost, String comment) {
		
		Node partSup = gds.createNode();
		partSup.setProperty("psPartSupKey", partKey + supKey);
		partSup.setProperty("psAvailQty", availQty);
		partSup.setProperty("psSupplyCost", supplyCost);
		partSup.setProperty("psComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node part  = autoNodeIndex.get("pPartKey", partKey).getSingle();
		partSup.createRelationshipTo(part, RelTypes.HAS_PART);
		
		autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node supplier = autoNodeIndex.get("sSupKey", supKey).getSingle();
		partSup.createRelationshipTo(supplier, RelTypes.HAS_SUPPLIER);
		
	}
	
	private static void insertLineItem(GraphDatabaseService gds, String orderKey,
			String partKey, String supKey, String lineNumber, long quantity,
			long extendedPrice, long discount, long tax, String returnFlag,
			String lineStatus, long shipDate, long commitDate, long receiptDate,
			String shipInstruct, String shipMode, String comment) {
		
		Node lineItem = gds.createNode();
		
		lineItem.setProperty("liLineNumber", lineNumber);
		lineItem.setProperty("liQuantity", quantity);
		lineItem.setProperty("liExtendedPrice", extendedPrice);
		lineItem.setProperty("liDiscount", discount);
		lineItem.setProperty("liTax", tax);
		lineItem.setProperty("liReturnFlag", returnFlag);
		lineItem.setProperty("liLineStatus", lineStatus);
		lineItem.setProperty("liShipDate", shipDate);
		lineItem.setProperty("liCommitDate", commitDate);
		lineItem.setProperty("liReceiptDate", receiptDate);
		lineItem.setProperty("liShipInstruct", shipInstruct);
		lineItem.setProperty("liShipMode", shipMode);
		lineItem.setProperty("liComment", comment);
		
		ReadableIndex<Node> autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node order  = autoNodeIndex.get("oOrderKey", orderKey).getSingle();
		lineItem.createRelationshipTo(order, RelTypes.HAS_ORDER);
		
		autoNodeIndex = gds.index().getNodeAutoIndexer().getAutoIndex();
		Node partSup  = autoNodeIndex.get("psPartSupKey", partKey + supKey).getSingle();
		lineItem.createRelationshipTo(partSup, RelTypes.HAS_PART_SUP);
		
	}

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		
		double sf = 0.0066667;
        //boolean indexes = true;
        int rango_supplier = 0;
        int rango_customer = 0;
        int rango_parts = 0;
        int rango_orders = 0;
        int rango_lineitem = 0;
		
		Map<String, String> config = new HashMap<String, String>();
		config.put( "neostore.nodestore.db.mapped_memory", "10M" );
		config.put( "string_block_size", "60" );
		config.put( "array_block_size", "300" );
		GraphDatabaseService c = new GraphDatabaseFactory()
		    .newEmbeddedDatabaseBuilder( "target/database/data" )
		    .setConfig( config )
		    .newGraphDatabase();
		
		registerShutdownHook(c);
		
		Transaction tx = c.beginTx();
		long t1, t2;
		try {
			AutoIndexer<Node> nodeAutoIndexer = c.index()
			        .getNodeAutoIndexer();
			nodeAutoIndexer.startAutoIndexingProperty( "rRegionKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "nNationKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "sSupKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "cCustKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "pPartKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "oOrderKey" );
			nodeAutoIndexer.startAutoIndexingProperty( "psPartSupKey" );
			
			nodeAutoIndexer.setEnabled(true);		
			
			insertsRegion(c);
			insertsNation(c);
			t1 = System.currentTimeMillis();
	        insertsSupplier(c,sf,rango_supplier);
	        insertsCustomer(c,sf,rango_customer);
	        insertsParts(c,sf,rango_parts);
	        insertsPartSupp(c,sf,rango_parts,rango_supplier);
	        insertsOrders(c,sf,rango_orders);
	        insertsLineItems(c,sf,rango_lineitem,rango_parts,rango_supplier);
	        
	        tx.success();
		} finally {
			tx.finish();
		}
        
		t2 = System.currentTimeMillis();
		
		System.out.println("Inserts batch 1 - " + String.valueOf(t2 - t1) );
		
		ExecutionEngine ee = new ExecutionEngine(c);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long date = sdf.parse("2006-06-01").getTime();
		
		String query1 = "START root=node(*) " +
				"WHERE HAS(root.liLineNumber) AND root.liShipDate <= " + Long.toString(date) + " " +
				"RETURN root.liReturnFlag, root.liLineStatus, SUM(root.liQuantity) AS sumQty, " +
				"SUM(root.liExtendedPrice) AS sumBasePrice, " +
				"SUM(root.liExtendedPrice*(1-root.liDiscount) ) AS sumDiscPrice, " +
				"SUM(root.liExtendedPrice*(1-root.liDiscount)*(1+root.liTax) ) AS sumCharge, " +
				"AVG(root.liQuantity) AS avgQty, AVG(root.liExtendedPrice) AS avgPrice, " +
				"AVG(root.liDiscount) AS avgDisc, COUNT(*) AS countOrder " +
				"ORDER BY root.liReturnFlag, root.liLineStatus";
		
		t1 = System.currentTimeMillis();
		
		tx = c.beginTx();
		try {
			ee.execute(query1);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 1 - " + String.valueOf(t2 - t1) );
		
		String query2 = "START r=node(*) " +
				"MATCH (r)-[:HAS_REGION]-(n)-[:HAS_NATION]-(s)-[:HAS_SUPPLIER]-(ps)-[:HAS_PART]-(p) " +
				"WHERE HAS(r.rName) AND r.rName = 'xaimkctvqnsdpjfzmdvoziahtcckypnk' " +
				"WITH r, n, s, ps, p, min(ps.psSupplyCost) AS minim " +
				"WHERE HAS(n.nNationKey) " +
				"AND HAS(s.sSupKey) " +
				"AND HAS(ps.psPartSupKey) AND ps.psSupplyCost = minim " +
				"AND HAS(p.pPartKey) AND p.pSize = '555555555555555555731605' AND p.pType =~ '.*pxr' " +				
				"RETURN s.sAcctBal, s.sName, n.nName, p.pPartKey as pPartKeyExtra, p.pMfgr, p.pType as pTypeExtra, minim, s.sSupKey, s.sAddress, s.sPhone, s.sComment " +
				"ORDER BY s.sAcctBal DESC, n.nName, s.sName, p.pPartKey";
		
		t1 = System.currentTimeMillis();
		
		tx = c.beginTx();
		try {		
			ee.execute(query2);
			tx.success();
		} finally {
			tx.finish();
		}
			
		t2 = System.currentTimeMillis();
		System.out.println("Query 2 - " + String.valueOf(t2 - t1) );

		date = sdf.parse("2006-06-01").getTime();
		
		String query3 = "START c=node(*) " +
				"MATCH (li)-[:HAS_ORDER]->(o)-[:HAS_CUSTOMER]->(c) " +
				"WHERE HAS(li.liExtendedPrice) AND li.liShipDate > " + Long.toString(date) + " " +
				"AND HAS(c.cCustKey) AND c.cMktSegment = 'ifsxmyyotzihrrlynmgdufrzqnwoezoq' " +
				"AND HAS(o.oOrderKey) AND o.oOrderDate < " + Long.toString(date) + " " +
				"WITH o, sum(li.liExtendedPrice*(1-li.liDiscount)) AS revenue " +
				"RETURN o.oOrderKey, revenue, o.oOrderDate, o.oShipPriority " +
				"ORDER BY revenue DESC, o.oOrderDate";
		
		t1 = System.currentTimeMillis();
		tx = c.beginTx();
		try {
			ee.execute(query3);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 3 - " + String.valueOf(t2 - t1) );
		
		date = sdf.parse("2006-06-01").getTime();
		long date1 = sdf.parse("2007-06-01").getTime();
		
		String query4 = "START r=node(*) " +
				"MATCH (r)-[:HAS_REGION]-(n)," +
				"(n)-[:HAS_NATION]-(s)-[:HAS_SUPPLIER]-(ps)-[:HAS_PARTSUP]-(li), " +
				"(n)-[:HAS_NATION]-(c)-[:HAS_CUSTOMER]-(o)-[:HAS_ORDER]-(li) " +
				"WHERE HAS(r.rName) AND r.rName = 'xaimkctvqnsdpjfzmdvoziahtcckypnk' " +
				"AND HAS(s.sSupKey) " +
				"AND HAS(c.cCustKey) " +
				"AND HAS(o.oOrderDate) AND o.oOrderDate >= " + Long.toString(date) + " " +
						"AND o.oOrderDate < " + Long.toString(date1) + " " +
				"RETURN n.nName, sum(li.liExtendedPrice*1-li.liDiscount) AS revenue " +
				"ORDER BY revenue";
		
		t1 = System.currentTimeMillis();
		tx = c.beginTx();
		try {
			ee.execute(query4);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 4 - " + String.valueOf(t2 - t1) );		

		rango_supplier += sf*10000/2;
        rango_customer += sf*150000/2;
        rango_orders += sf*1500000/2;
        rango_parts += sf*200000/2;
        rango_lineitem += sf*6000000/2;
        
        tx = c.beginTx();
        try {
        	t1 = System.currentTimeMillis();
        	insertsSupplier(c,sf,rango_supplier);
	        insertsCustomer(c,sf,rango_customer);
	        insertsParts(c,sf,rango_parts);
	        insertsPartSupp(c,sf,rango_parts,rango_supplier);
	        insertsOrders(c,sf,rango_orders);
	        insertsLineItems(c,sf,rango_lineitem,rango_parts,rango_supplier);        	
        	tx.success();
        }
        finally {
        	tx.finish();
        }
        t2 = System.currentTimeMillis();
        
        System.out.println("Inserts batch 2 - " + String.valueOf(t2 - t1) );
		
		t1 = System.currentTimeMillis();
		
		tx = c.beginTx();
		try {
			ee.execute(query1);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 1 - " + String.valueOf(t2 - t1) );

		t1 = System.currentTimeMillis();
		
		tx = c.beginTx();
		try {		
			ee.execute(query2);
			tx.success();
		} finally {
			tx.finish();
		}
			
		t2 = System.currentTimeMillis();
		System.out.println("Query 2 - " + String.valueOf(t2 - t1) );
		
		t1 = System.currentTimeMillis();
		tx = c.beginTx();
		try {
			ee.execute(query3);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 3 - " + String.valueOf(t2 - t1) );
		
		t1 = System.currentTimeMillis();
		tx = c.beginTx();
		try {
			ee.execute(query4);
			tx.success();
		} finally {
			tx.finish();
		}
		
		t2 = System.currentTimeMillis();
		System.out.println("Query 4 - " + String.valueOf(t2 - t1) );
	
        c.shutdown();

	}
	
	private static void insertsRegion(GraphDatabaseService c) {
        /*        REGION Table Layout
        Column Name     Datatype Requirements       Comment
        R_REGIONKEY     number(38,0)                 5 regions are populated
        R_NAME          varchar2(64)         
        R_COMMENT       variable text, size 152
        Primary Key: R_REGIONKEY*/
        Random random = new Random(42);
        for (int i = 0; i < 5; ++i) {
            //System.out.println(i);
            String regionKey = "1000000000000000000"+Integer.toString(i);
            String regionName = randomString(32,random);
            String comment =  randomString(80,random);
            insertRegion(c, regionKey, regionName, comment);
            
        }
    }
	
    private static void insertsNation(GraphDatabaseService c) {
        /*Column Name     Datatype Requirements       Comment
        N_NATIONKEY       Number(38,0)                25 nations are populated
        N_NAME            Varchar2(64)         
        N_REGIONKEY       Number(38,0)                Foreign Key to R_REGIONKEY  
        N_COMMENT         Varchar(160)     
        Primary Key: N_NATIONKEY*/
        Random myrandom = new Random(42);
        
        for (int i = 0; i < 25; ++i) {
        	insertNation(
        		c,
    			"1000000000000000000"+Integer.toString(i),
        		randomString(32,myrandom),
        		"1000000000000000000"+myrandom.nextInt(5),
        		randomString(80,myrandom)
            );
        }
    }
    
    private static void insertsSupplier(GraphDatabaseService c, double sf, int par) {
/*       Column Name    Datatype Requirements   Comment
 *       S_SUPPKEY      Number(38,0)              SF*10,000 are populated
 *       S_NAME         Varchar2(64)
 *       S_ADDRESS      Varchar2(64)
 *       S_NATIONKEY    Number(38,0)              Foreign Key to N_NATIONKEY
 *       S_PHONE        Varchar2(18)     
 *       S_ACCTBAL      Number(13,2)                 
 *       S_COMMENT      Varchar2(105)
 *       Primary Key: S_SUPPKEY*/
        Random random = new Random(par*42);

        for (int i = 0+par; i < ((int)(sf*10000/2)*(par>0?2:1)); ++i) {
          //  System.out.println(i-par);
        	insertSupplier(
        		c,
		        "1000000000000000000"+Integer.toString(i),
		        randomString(32,random),
		        randomString(32,random),
		        "1000000000000000000"+Integer.toString(random.nextInt(25)),
		        randomString(9,random),
		        "99999"+Integer.toString(random.nextInt(99999)),
		        randomString(52,random)
           );
        }

    }

    private static void insertsCustomer(GraphDatabaseService c, double sf, int par) {
/*      Column Name     Datatype Requirements   Comment
 *      C_CUSTKEY       Number(38,0)              SF*150,000 are populated
 *      C_NAME          Varchar2(64)
 *      C_ADDRESS       Varchar2(64)
 *      C_NATIONKEY     Number(38,0)              Foreign Key to N_NATIONKEY
 *      C_PHONE         Varchar2(64)
 *      C_ACCTBAL       Number(13,2)
 *      C_MKTSEGMENT    Varchar2(64)
 *      C_COMMENT       Varchar2(120)
 *      Primary Key: C_CUSTKEY
 *
 *
 */
        Random random = new Random(par*42);
        
        for (int i = 0+par; i < ((int)(sf*150000/2)*(par>0?2:1)); ++i) {
        	insertCustomer(
        			c,
	             "1000000000000000000"+Integer.toString(i),
	             randomString(32,random),
	             randomString(32,random),
	             "1000000000000000000"+Integer.toString(random.nextInt(25)),
	             randomString(32,random),
	             "99999"+Integer.toString(random.nextInt(99999)),
	             randomString(32,random),
	             randomString(60,random)
             );
        }
        
    }

    private static void insertsParts(GraphDatabaseService c, double sf, int par) {
/*      Column Name     Datatype Requirements   Comment
 *      P_PARTKEY       Number(38,0)            SF*200,000 are populated
 *      P_NAME          Varchar2(64)
 *      P_MFGR          Varchar2(64)
 *      P_BRAND         Varchar2(64)
 *      P_TYPE          Varchar2(64)
 *      P_SIZE          Number(38,0)
 *      P_CONTAINER     Varchar2(64)
 *      P_RETAILPRICE   Number(13,2)
 *      P_COMMENT       Varchar2(64)
 *      Primary Key: P_PARTKEY*/
        Random random = new Random(par*42);
         for (int i = 0+par; i < ((int)(sf*200000/2)*(par>0?2:1)); ++i) {
        	 insertPart(
        			 c,
	             "1000000000000000000"+Integer.toString(i),
	             randomString(32,random),
	             randomString(32,random),
	             randomString(32,random),
	             randomString(32,random),
	             "555555555555555555"+Integer.toString(random.nextInt(999999)),
	             randomString(32,random),
	             "77777"+Integer.toString(random.nextInt(99999)),
	             randomString(32,random)
             );
         }

    }

    private static void insertsPartSupp(GraphDatabaseService c, double sf, int parP, int parS) {
/*      Column Name     Datatype Requirements   Comment
 *      PS_PARTKEY      Number(38,0)              Foreign Key to P_PARTKEY
 *      PS_SUPPKEY      Number(38,0)              Foreign Key to S_SUPPKEY
 *      PS_AVAILQTY     Number(38,0)
 *      PS_SUPPLYCOST   Number(13,2)
 *      PS_COMMENT      Varchar2(200)
 *      Primary Key: PS_PARTKEY, PS_SUPPKEY*/
        
        int s_max;
        s_max = ((int) (sf*10000)/2)*(parP > 0 ? 2 : 1);
        int p,s;
        p = parP;
        s = parS;
        Random random = new Random(parP*42);
        
        for (int i = 0; i < ((int)(sf*800000/2)*(parP>0?2:1)); ++i) {
        	insertPartSup(
    			c,
	            "1000000000000000000"+Integer.toString(p),
	            "1000000000000000000"+Integer.toString(s),
	            "111111111111111111"+random.nextInt(99999),
	            "77777"+Integer.toString(random.nextInt(99999)),
	            randomString(100,random)
	        );
            ++s;
            if (s >= s_max) {
                s = parS;
                ++p;
            }
            
        }
    }

    private static void insertsOrders(GraphDatabaseService c, double sf, int par) throws ParseException {
/*      Column Name     Datatype Requirements       Comment
 *      O_ORDERKEY      Number(38,0)                SF*1,500,000 are sparsely populated
 *      O_CUSTKEY       Number(38,0)                Foreign Key to C_CUSTKEY
 *      O_ORDERSTATUS   Varchar2(64)
 *      O_TOTALPRICE    Number(13,2)
 *      O_ORDERDATE     Date
 *      O_ORDERPRIORITY Varchar2(15)
 *      O_CLERK         Varchar2(64)
 *      O_SHIPPRIORITY  Number(38,0)
 *      O_COMMENT       Varchar2(80)
 *      Primary Key: O_ORDERKEY*/
        
        Random random = new Random(par*42);
        for (int i = 0+par; i < ((int)(sf*1500000/2)*(par>0?2:1)); ++i) {
        	insertOrder(
        			c,
		            "1000000000000000000"+Integer.toString(i),
		            "1000000000000000000"+Integer.toString(random.nextInt(2*((int)(sf*150000/2)*(1+(par>0?1:0)))/3)),
		            randomString(32,random),
		            "22222"+Integer.toString(random.nextInt(999999)),
		            randomDate(random),
		            randomString(7,random),
		            randomString(32,random),
		            "333333333333333333"+Integer.toString(random.nextInt(999999)),
		            randomString(40,random)
		    );
        }
    }

    private static void insertsLineItems(GraphDatabaseService c, double sf, int par, int parP, int parS) throws NumberFormatException, ParseException {
 /*     Column Name     Datatype Requirements       Comment
  *  1   L_ORDERKEY      Number(38,0)                Foreign Key to O_ORDERKEY
  *  2   L_PARTKEY       Number(38,0)                Foreign key to P_PARTKEY, first part of the compound Foreign Key to (PS_PARTKEY, PS_SUPPKEY) with L_SUPPKEY
  *  3   L_SUPPKEY       Number(38,0)                Foreign key to S_SUPPKEY, second part of the compound Foreign Key to (PS_PARTKEY, PS_SUPPKEY) with L_PARTKEY
  *  4   L_LINENUMBER    Number(38,0)                 
  *  5   L_QUANTITY      Number(38,0)
  *  6   L_EXTENDEDPRICE Number(13,2)
  *  7   L_DISCOUNT      Number(13,2)
  *  8   L_TAX           Number(13,2)
  *  9   L_RETURNFLAG    Varchar2(64)
  *  10   L_LINESTATUS    Varchar2(64)
  *  11   L_SHIPDATE      date
  *  12   L_COMMITDATE    date
  *  13   L_RECEIPTDATE   date
  *  14   L_SHIPINSTRUCT  Varchar2(64)
  *  15   L_SHIPMODE      Varchar2(64)
  *  16   L_COMMENT       Varchar2(64)
  *     Primary Key: L_ORDERKEY, L_LINENUMBER*/
        int s_max, ps_max;
        s_max = ((int) (sf*10000/2)*(par>0?2:1) );
        ps_max = ((int)(sf*800000/2)*(parP>0?2:1));
        int p,s, ps;
        p = parP;
        s = parS;
        ps = p*s;
        Random random = new Random(42);
        
        for (int i = 0+par; i < ((int)(sf*6000000/2)*(par>0?2:1)); ++i) {
        	insertLineItem(
        		c,
	            "1000000000000000000"+(random.nextInt((int)(sf*1500000/2)*(1+(par>0?1:0)))),
	            "1000000000000000000"+Integer.toString(p),
	            "1000000000000000000"+Integer.toString(s),
	            "1000000000000000000"+Integer.toString(i),
	            Long.parseLong("0000"+Integer.toString(random.nextInt(999999999) ) ),
	            Long.parseLong("2222"+Integer.toString(random.nextInt(999999999) ) ),
	            Long.parseLong("3333"+Integer.toString(random.nextInt(999999999) ) ),
	            Long.parseLong("4444"+Integer.toString(random.nextInt(999999999) ) ),
	            randomString(32,random),
	            randomString(32,random),
	            randomDate(random),
	            randomDate(random),
	            randomDate(random),
	            randomString(32,random),
	            randomString(32,random),
	            randomString(32,random)
            );
            ++s;
            ++ps;
            
            if (s >= s_max) {
                s = parS;
                ++p;
            }
            
            if (ps >= ps_max) {
            	s = parS;
            	p = parP;
            }
            
        }
    }
    
    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }

}
